package user_input;

import file_service.InvalidFileFormattingException;
import matrix.BinaryOperators;
import matrix.InvalidMatrixSizeException;
import matrix.SparseMatrix;
import matrix.UnaryOperators;

import java.io.IOException;


public class UserInputController {
    public static void handleUserInputForMatrixOperations(String[] args){
        // format: matA.txt matB.txt mat_res.txt x
        final String FORMAT_MESSAGE = "Pass arguments in format: matA.txt matB.txt [mat_result.txt] operator\n" +
                "If operator is a unary operator, matB.txt should not be passed (3 arguments max.)";
        if ( args.length < 2 || args.length > 4){
            System.err.println(FORMAT_MESSAGE);
        }
        int argsLength = args.length;
        String operatorString = args[argsLength-1];
        try{
            UnaryOperators uo = getUnaryOperatorValue(operatorString);
            if ( args.length > 3){
                System.err.println(FORMAT_MESSAGE);
            }
            String spStr = args[0];
            String resStr = argsLength == 3 ? args[1] : null;
            performUnaryOperationOn(uo, spStr, resStr);

        } catch (IllegalArgumentException e){
            try{
                BinaryOperators bo = getBinaryOperatorValue(operatorString);
                String sp1Str = args[0];
                String sp2Str = args[1];
                String resStr = argsLength == 4 ? args[2] : null;
                performBinaryOperationOn(bo, sp1Str, sp2Str, resStr);

            } catch(IllegalArgumentException e2){
                System.err.println("Please enter valid operator: " + UnaryOperators.values() + " or: " + BinaryOperators.values());
            }
        }

    }
    private static UnaryOperators getUnaryOperatorValue(String argument) throws IllegalArgumentException {
        for ( UnaryOperators op : UnaryOperators.values()) {
            if (argument.equals(op.getRepresentation())) {
                return op;
            }
        }
        throw new IllegalArgumentException();
    }

    private static BinaryOperators getBinaryOperatorValue(String argument) throws IllegalArgumentException {
        for ( BinaryOperators op : BinaryOperators.values()) {
            if (argument.equals(op.getRepresentation())) {
                return op;
            }
        }
        throw new IllegalArgumentException();
    }

    private static void performUnaryOperationOn(UnaryOperators operator, String sp1Str, String resStr){
        try{
            SparseMatrix spRes = operator.perform(new SparseMatrix(sp1Str));
            if (resStr != null){
                spRes.setPath(resStr);
            } else {
                spRes.setPath(operator.getDescription()+".txt");
            }
            spRes.saveMatrixToFileOverwrite();

        } catch (InvalidFileFormattingException e){
            System.err.println("File improperly formatted");
        } catch (IOException e){
            System.err.println("Couldn't open the file");

        }

    }

    private static void performBinaryOperationOn(BinaryOperators operator, String sp1Str, String sp2Str, String resStr){
        try {
            SparseMatrix spRes = operator.perform(new SparseMatrix(sp1Str), new SparseMatrix(sp2Str));
            if (resStr == null) {
                resStr = operator.getDescription() + ".txt";
            }
            spRes.setPath(resStr);
            spRes.saveMatrixToFileOverwrite();
        } catch(InvalidMatrixSizeException e) {
            System.err.println("Coudln't perform operation due to non-matching matrices size");
        } catch (InvalidFileFormattingException e){
            System.err.println("File improperly formatted");
        } catch (IOException e){
            System.err.println("Couldn't open the file");

        }

    }



}
