import file_service.InvalidFileFormattingException;
import matrix.InvalidMatrixSizeException;
import matrix.SparseMatrix;
import user_input.UserInputController;

import java.io.IOException;

public class Main {

    public static void main(String... args) {
        /*
        Tree tree = new Tree();
        tree.insert(9, 2.2);
        tree.insert(8, 3.1);
        tree.insert(5, 5.4);
        tree.insert(10, 99.9);
        tree.insert(7, 1.0101);
        TreeDump.dumpEachLevel(tree);
       */

       /* try {
            tree.delete(10);
            TreeDump.dumpEachLevel(tree);
            tree.delete(9);
            TreeDump.dumpEachLevel(tree);
            tree.deleteMin();
            TreeDump.dumpEachLevel(tree);
            tree.deleteMin();
            TreeDump.dumpEachLevel(tree);
            tree.deleteMin();
            TreeDump.dumpEachLevel(tree);
        } catch (IndexNotFoundInTreeException e){
            System.err.println("Some index not found");
            e.printStackTrace();
        }
        try {
            tree.getValueOnIndex(5);
            tree.getValueOnIndex(10);
            tree.getValueOnIndex(7);
            tree.getValueOnIndex(8);
            tree.getValueOnIndex(9);
        } catch (IndexNotFoundInTreeException e) {
            System.err.println("Index not found");
        }
        */


        /*

        tree = new Tree();
        Random rand = new Random();
        int limit = rand.nextInt(10)+10;
        for ( int i = 0; i < limit; i++ ){
            tree.insert(rand.nextInt(1000), rand.nextDouble()*5-5/2.);
        }
        TreeDump.dumpEachLevel(tree);
        while ( !tree.isEmpty() ){
            try{
                tree.delete(rand.nextInt(1000));
            } catch (IndexNotFoundInTreeException e){
                try{
                    tree.deleteMin();
                } catch (IndexNotFoundInTreeException e2) {
                    System.err.println("Too much deletion performed!");
                }
            }
            TreeDump.dumpEachLevel(tree);
            System.out.println( "size: " + tree.getSize() );
            System.out.println( "depth: " + tree.getDepth() );
        }


        tree = new Tree();
        for (int i = 0; i < 10; i++) {
            tree.insert(rand.nextInt(100), rand.nextDouble());
        }
        TreeDump.dump(tree);

        SparseVector vector = new SparseVector();
        try {
            vector.insert(12.2);
            vector.insert(12.123);
            vector.insert(453);
            vector.insert(1423);
            vector.insert(12);
        } catch (ZeroInsertingException e) {
            System.err.println("Cannot insert zero!");
        }
        System.out.println(vector.toString());
        System.out.println("second element: "+vector.get(1));

        vector = new SparseVector();
        for (int i = 0; i < 5; i++) {
            try {
                vector.insert(rand.nextInt(200), 50*(int)(rand.nextDouble()*10e4)/(double)10e4);
            } catch (ZeroInsertingException e) {
                System.err.println("Cannot insert to vector!");
            }
        }
        System.out.println(vector.toString());

        vector = new SparseVector();
        try {
            vector.insert(4, 12.2);
            vector.insert(3, 0.001);
        } catch ( ZeroInsertingException e ){
            System.err.println("Cannot insert zero");
        }
        System.out.println(vector.toString());
        System.out.println("index:3, value: "+vector.get(3));

        */


/**
 * Matrix operations
 */
/*
        try {
            SparseMatrix sp = new SparseMatrix("matA.txt");
            System.out.println(sp.toString());
            SparseMatrix spT = sp.transposeMatrix();
            System.out.println(spT.toString());
            spT.setPath("matA_transposed.txt");
            spT.saveMatrixToFileOverwrite();
        } catch (InvalidFileFormattingException e){
            System.err.println("File matA.txt is not in desired format");
        } catch (IOException e){
            System.err.println("A problem while reading from a matA.txt file occured");
        }
        try {
            SparseMatrix sp = new SparseMatrix("matA.txt");
            SparseMatrix spB = new SparseMatrix("matB.txt");
            SparseMatrix spAplusB = sp.addMatrix(spB);
            System.out.println(spAplusB.toString());
            spAplusB.setPath("matA_plus_matB.txt");
            spAplusB.saveMatrixToFileOverwrite();
        } catch (InvalidFileFormattingException e){
            System.err.println("File matA.txt or matB.txt is not in desired format");
        } catch (IOException e){
            System.err.println("A problem while reading from a matA.txt or matB.txt file occured");
        }

        try{
            try {
                SparseMatrix sp = new SparseMatrix("matA.txt");
                SparseMatrix spB = new SparseMatrix("matB.txt");
                SparseMatrix spAtimesB = sp.multiplyMatrix(spB);
                System.out.println(spAtimesB.toString());
                spAtimesB.setPath("matA_times_matB.txt");
                spAtimesB.saveMatrixToFileOverwrite();
            } catch (InvalidMatrixSizeException e) {
                System.err.println("Matrix dimensions don't match. Operation not executed.");
            }
        } catch (InvalidFileFormattingException e){
            System.err.println("File matA.txt or matB.txt is not in desired format");
        } catch (IOException e){
            System.err.println("A problem while reading from a matA.txt or matB.txt file occured");
        }

       */




        /**
         *
         * Console interaction with the user
         *
         */

        // comment rest, uncomment this for command-line interaction
        UserInputController.handleUserInputForMatrixOperations(args);



    }
}
