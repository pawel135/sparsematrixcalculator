package matrix;

import file_service.FileParser;
import file_service.FileService;
import file_service.InvalidFileFormattingException;
import vector.IndexNotInVectorException;
import vector.InvalidVectorSizeException;
import vector.SparseVector;
import vector.ZeroInsertingException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class SparseMatrix {
    Vector<SparseVector> content;
    Vector<Vector<Integer>> indexesMap;
    Path path;
    private FileService fileService;
    int numberOfRows = 0;
    int numberOfColumns = 0;

    public SparseMatrix(Path p) throws InvalidFileFormattingException, IOException{
        fileService = new FileService(p);
        this.path = p;
        try{
            numberOfRows = FileParser.getNumberOfRows(fileService.getFirstLine());
            numberOfColumns = FileParser.getNumberOfColumns(fileService.getFirstLine());
        } catch (InvalidFileFormattingException e){
            numberOfRows = 0;
            numberOfColumns = 0;
        }
        this.content = getFileContent(); // throws InvalidFileFormattingException
        this.indexesMap = makeIndexesMap();
    }

    public SparseMatrix(File f) throws InvalidFileFormattingException, IOException{
        this(Paths.get(f.getPath()));
    }

    public SparseMatrix(String s) throws InvalidFileFormattingException, IOException{
        this(Paths.get(s));
    }

    public SparseMatrix(int rows, int cols, Path resultPath) throws IOException{
        this.path = resultPath;
        this.numberOfRows = rows;
        this.numberOfColumns = cols;
        fileService = new FileService(resultPath);
    }

    public SparseMatrix(Vector<SparseVector> content, int rows, int cols, Path resultPath) throws IOException, IllegalArgumentException{
        this.path = resultPath;
        this.content = content;
        if ( rows != content.size() ){
            throw new IllegalArgumentException();
        }
        numberOfRows = rows;
        numberOfColumns = cols;
        this.indexesMap = makeIndexesMap();

        fileService = new FileService(resultPath);
    }

    public SparseMatrix(Vector<SparseVector> content, Vector<Vector<Integer>> indexesMap, int rows, int cols, Path resultPath) throws IOException, IllegalArgumentException{
        this.path = resultPath;
        this.content = content;
        this.indexesMap = indexesMap;
        if ( rows != content.size() || rows != indexesMap.size() ){
            throw new IllegalArgumentException();
        }
        numberOfRows = rows;
        numberOfColumns = cols;
        fileService = new FileService(resultPath);
    }


    public int getNumberOfColumns(){
        return numberOfColumns;
    }
    public int getNumberOfRows(){
        return numberOfRows;
    }

    public void setPath(String pathStr) throws IOException{
        Path p = Paths.get(pathStr);
        this.path = p;
        this.fileService = new FileService(this.path);
    }

    private Vector<Vector<Integer>> makeIndexesMap(){
        Vector<Vector<Integer>> indexesMap = new Vector<>(numberOfRows);
        for ( int row = 0; row < numberOfRows; row++) {
            Vector mapRow;
            try{
                mapRow = content.get(row).getIndexes();
            } catch (NullPointerException e){
                //It's ok as null means no non-zero elements in a row
                mapRow = null;
            }
            indexesMap.add(mapRow);
        }
        return indexesMap;
    }

    private Vector<SparseVector> getFileContent() throws InvalidFileFormattingException, IOException{
        try{
            return FileParser.parseFileContent(fileService.getFileLines());
        } catch (ZeroInsertingException e){
            System.err.println("Specified matrix values may not be zeros");
        }
        return new Vector<>();
    }

    public void saveMatrixToFileOverwrite() throws IOException{
        this.path = fileService.saveFileOverwrite(content, numberOfRows,numberOfColumns);
        System.out.println(path);
    }

    public void saveMatrixToFileDontOverwrite() throws IOException{
        this.path = fileService.saveFileDontOverwrite(content, numberOfRows,numberOfColumns);
        System.out.println(path);
    }

    public Vector<SparseVector> getMatrixRepresentation(){
        return content;
    }


    public SparseVector getColumn(int column) throws IllegalArgumentException{
        if ( column < 0 || column >= numberOfColumns ){
            throw new IllegalArgumentException();
        }
        SparseVector result = new SparseVector();
        result.setSize(this.getNumberOfRows());
        double value;
        for ( int i = 0; i < this.getNumberOfRows(); i++){
            try{
                value = this.getMatrixRepresentation().get(i).get(column);
                try{
                    result.insert(i,value);
                } catch( ZeroInsertingException e){
                    System.err.println("Improper getColumn implementation");
                }
            } catch( IndexNotInVectorException e){
                //OK, may be zero
            }

        }
        return result;
    }


    public SparseMatrix transposeMatrix() throws IOException{
        Vector<SparseVector> resultContent = new Vector<>(numberOfColumns);
        Vector<Vector<Integer>> resultMap = new Vector<>(numberOfColumns);
        for ( int newRow = 0; newRow < numberOfColumns; newRow++){
            resultMap.add(newRow, new Vector<>());
            resultContent.add(newRow, new SparseVector());
        }

        for ( int row = 0; row < numberOfRows; row++){
            int i = 0;
            for ( Integer nonZeroColIndex :  indexesMap.get(row) ){
                resultMap.get(nonZeroColIndex).add(row);
                try{
                    resultContent.get(nonZeroColIndex).insert(row,content.get(row).get(nonZeroColIndex));
                } catch (ZeroInsertingException|IndexNotInVectorException e){
                    System.err.println("Improper tranposeMatrix implementation");
                }
            }

        }
        try {
            return new SparseMatrix(resultContent, resultMap, numberOfColumns, numberOfRows, this.path);
        } catch(IOException e){
            throw e;
        }
    }
    private Integer findIndexForValueInVector(Integer value, Vector<Integer> vector){
        if ( value == null || vector == null ){
            return null;
        }
        for ( int i=0; i < vector.size(); i++){
            if ( vector.get(i) > value ) {
                return i;
            }
        }
        return vector.size();
    }


    public SparseMatrix addMatrix(SparseMatrix other) throws IOException{
        int rowsA = this.numberOfRows;
        int colsA = this.numberOfColumns;
        int rowsB = other.numberOfRows;
        int colsB = other.numberOfColumns;
        int numberOfRows = Math.max(rowsA,rowsB);
        int numberOfColumns = Math.max(colsA, colsB);
        Vector<SparseVector> resultContent = new Vector<>(numberOfRows);
        Vector<Vector<Integer>> resultMap = new Vector<>(numberOfRows);

        // make resultMap
        for ( int newRow = 0; newRow < numberOfRows; newRow++){
            resultMap.add(newRow, new Vector<>());
            resultContent.add(newRow, new SparseVector());
            if ( this.indexesMap.size() > newRow ) {
                resultMap.get(newRow).addAll(this.indexesMap.get(newRow));
            }
            if ( other.indexesMap.size() > newRow ) {
                for (int otherCol : other.indexesMap.get(newRow)) {
                    if (!resultMap.get(newRow).contains(otherCol)) {
                        int x = findIndexForValueInVector(otherCol,resultMap.get(newRow));
                        if ( x == resultMap.get(newRow).size()){
                            resultMap.get(newRow).add(otherCol);
                        } else {
                            resultMap.get(newRow).insertElementAt(otherCol, x);
                        }
                    }
                }
            }
        }

        //make resultContent
        for ( int row = 0; row < numberOfRows; row++){
            for ( Integer nonZeroColIndex :  resultMap.get(row) ) {
                double value;
                try {
                    if (this.content.size() > row && this.content.get(row).contains(nonZeroColIndex)
                            && other.content.size() > row && other.content.get(row).contains(nonZeroColIndex)) {
                        value = this.content.get(row).get(nonZeroColIndex) + other.content.get(row).get(nonZeroColIndex);
                    } else if (this.content.size() > row && this.content.get(row).contains(nonZeroColIndex)) {
                        value = this.content.get(row).get(nonZeroColIndex);
                    } else {
                        value = other.content.get(row).get(nonZeroColIndex);
                    }
                    try {
                        resultContent.get(row).insert(nonZeroColIndex, value);
                    } catch (ZeroInsertingException e){
                        //It's OK as long as it comes from adding two values, nothing will be inserted
                    }
                } catch (IndexNotInVectorException e){
                    System.err.println("Improper addMatrix Implementation");
                }
            }
        }

        try {
            String resultName = FileService.makeFilename(this.path.toString(), other.path.toString(), "_add_");
            return new SparseMatrix(resultContent, resultMap, numberOfRows, numberOfColumns, Paths.get(resultName) );
        } catch(IOException e){
            throw e;
        }
    } //addMatrix


    public SparseVector multiplyVector(SparseVector other) throws InvalidVectorSizeException {
        //TODO:
        if ( this.getNumberOfColumns() != other.getSize() ){
            throw new InvalidVectorSizeException();
        }
        int r = this.getNumberOfRows();
        int c = this.getNumberOfColumns();
        SparseVector result = new SparseVector();
        result.setSize(r);
        double value;
        for ( int i = 0; i < r; i++){
            value = this.getMatrixRepresentation().get(i).multiplyGetScalar(other);
            try{
                result.insert(i,value);
            } catch(ZeroInsertingException e){
                //OK as the result may be 0
            }
        }

        return result;
    }


    public SparseMatrix multiplyMatrix(SparseMatrix other) throws InvalidMatrixSizeException, IOException{

        int numberOfRows = this.numberOfRows;
        int numberOfColumns = other.numberOfColumns;
        if ( this.numberOfColumns != other.numberOfRows ){
            throw new InvalidMatrixSizeException();
        }
        Vector<SparseVector> resultContent = new Vector<>(numberOfRows);
        Vector<Vector<Integer>> resultMap = new Vector<>(numberOfRows);

         //make resultContent
        for ( int row = 0; row < numberOfRows; row++){
            resultMap.add(row, new Vector<>());
            resultContent.add(row, new SparseVector());
            for ( int i: this.indexesMap.get(row) ) {
                for (int col : other.indexesMap.get(i) ){
                    resultMap.get(row).add(col);
                    try{
                        resultContent.get(row).insert(col, this.content.get(row).get(i) * other.content.get(i).get(col) );
                    } catch (ZeroInsertingException e){
                        //OK, It won't be entered. It should not happen unless both numbers are small
                    } catch (IndexNotInVectorException e){
                        System.err.println("Improper multiplyMatrix implementation");
                    }
                }
            }
        }

        try {
            String resultName = FileService.makeFilename(this.path.toString(), other.path.toString(), "_multiply_");
            return new SparseMatrix(resultContent, resultMap, numberOfRows, numberOfColumns, Paths.get(resultName) );
        } catch(IOException e){
            throw e;
        }
    }

    //TODO:
    public SparseMatrix invertMatrix(){
        return null;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        final String SEPARATOR = ", ";
        sb.append(String.format("%3s| index->value","row")+"\n");
        for ( int row = 0; row < content.size() ; row++){
//        for ( SparseVector sv : content){
                sb.append(String.format("%3d| ",row));
                String separator = "";
                int i = 0;
                for ( int contentIndex : content.get(row).getIndexes()){
                    sb.append(separator);
                    try{
                        sb.append(indexesMap.get(row).get(i) + "->" +  content.get(row).get(contentIndex));
                    } catch (IndexNotInVectorException e){
                        System.err.println("Invalid indexesMap creation");
                    }
                    separator = SEPARATOR;
                    i++;
                }
                sb.append("\n");
        }
        return sb.toString();
    }

}
