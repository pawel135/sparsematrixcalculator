package matrix;

import java.io.IOException;

public enum BinaryOperators {
    ADD("+","sum"){
        @Override
        public SparseMatrix perform(SparseMatrix sp1, SparseMatrix sp2) throws IOException{
            return sp1.addMatrix(sp2);
        }
    },
    MULTIPLY("x","multiplication"){
        @Override
        public SparseMatrix perform(SparseMatrix sp1, SparseMatrix sp2) throws InvalidMatrixSizeException, IOException{
            return sp1.multiplyMatrix(sp2);
        }
    };

    private BinaryOperators(String representation, String description){
        this.representation = representation;
        this.description = description;
    }
    private String representation;
    private String description;
    public String getRepresentation(){
        return representation;
    }
    public String getDescription(){
        return description;
    }
    public abstract SparseMatrix perform(SparseMatrix sp1, SparseMatrix sp2) throws InvalidMatrixSizeException, IOException;
}
