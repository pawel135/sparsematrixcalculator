package matrix;

import java.io.IOException;

public enum UnaryOperators {
    TRANSPOSE("T" ,"tranposition"){
        @Override
        public SparseMatrix perform(SparseMatrix sp1) throws IOException{
            return sp1.transposeMatrix();
        }
    };

    private UnaryOperators(String representation, String description){
        this.representation = representation;
        this.description = description;
    }
    private String representation;
    private String description;
    public String getRepresentation(){
        return representation;
    }
    public String getDescription(){
        return description;
    }
    public abstract SparseMatrix perform(SparseMatrix sp1) throws IOException;
}
