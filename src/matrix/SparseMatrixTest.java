package matrix;

import org.junit.Before;
import org.junit.Test;
import vector.InvalidVectorSizeException;
import vector.SparseVector;
import vector.ZeroInsertingException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Vector;

import static org.junit.Assert.*;


public class SparseMatrixTest {
    SparseMatrix sparseMatrix;
    int rows = 5;
    int cols = 4;
    @Before
    public void setUp() throws Exception {
        Path resultPath = Paths.get("testMatrix");
        Vector<SparseVector> vectorOfSparseVectors = new Vector<>(rows);

        try {
            SparseVector matrixRow = new SparseVector(cols);
            matrixRow.insert(0, 10.0);
            matrixRow.insert(1, 15.0);
            matrixRow.insert(2, 5.0);
            matrixRow.insert(3, 4.0);
            vectorOfSparseVectors.insertElementAt(matrixRow, 0);

            matrixRow = new SparseVector(cols);
            matrixRow.insert(1, 2.0);
            matrixRow.insert(2, 3.0);
            matrixRow.insert(3, 4.0);
            vectorOfSparseVectors.insertElementAt(matrixRow, 1);

            matrixRow = new SparseVector(cols);
            matrixRow.insert(2, 5.0);
            vectorOfSparseVectors.insertElementAt(matrixRow, 2);

            matrixRow = new SparseVector(cols);
            matrixRow.insert(0, 1.0);
            matrixRow.insert(1, 2.0);
            matrixRow.insert(2, 6.0);
            matrixRow.insert(3, 4.0);
            vectorOfSparseVectors.insertElementAt(matrixRow, 3);

            matrixRow = new SparseVector(cols);
            matrixRow.insert(0, 5.0);
            matrixRow.insert(1, 2.0);
            matrixRow.insert(2, 2.0);
            matrixRow.insert(3, 2.0);
            vectorOfSparseVectors.insertElementAt(matrixRow, 4);
        } catch( ZeroInsertingException e){
            System.err.println("Improper implementation of sparseVector insert() or try to insert 0 in sparseMatrix tests setUp()");
        }
        sparseMatrix = new SparseMatrix(vectorOfSparseVectors,rows,cols,resultPath);
    }

    @Test
    public void testSaveMatrixToFileOverwrite() throws Exception {

    }

    @Test
    public void testSaveMatrixToFileDontOverwrite() throws Exception {

    }

    @Test
    public void testGetMatrixRepresentation() throws Exception {

    }

    @Test
    public void testGetColumn() throws Exception {
        SparseVector result = sparseMatrix.getColumn(0);

        assertTrue ( result.toString().equals("[10.0, 1.0, 5.0]") );
        assertTrue ( result.getIndexes().toString().equals("[0, 3, 4]") );
        assertTrue ( result.getSize() == rows );
    }

    @Test
    public void testTransposeMatrix() throws Exception {

    }

    @Test
    public void testAddMatrix() throws Exception {

    }

    @Test
    public void testMultiplyVector() throws Exception {
        SparseVector other = new SparseVector(cols);
        SparseVector result;
        try {
            other.insert(1.0);
            other.insert(2.0);
            other.insert(3, 3.0);
        } catch( ZeroInsertingException e ){

        }
        try {
            result = sparseMatrix.multiplyVector(other);
            assertTrue( result.toString().equals("[52.0, 16.0, 17.0, 15.0]") );
            assertTrue( result.getIndexes().toString().equals("[0, 1, 3, 4]") );
        } catch( InvalidVectorSizeException e){
            System.err.println("Invalid vector dimension in tests");
        }


    }

    @Test
    public void testMultiplyMatrix() throws Exception {

    }

    @Test
    public void testInvertMatrix() throws Exception {

    }

    @Test
    public void testToString() throws Exception {
        assertTrue(
                sparseMatrix.toString() + "is not equal to \n" +
                        String.format("%3s| index->value\n"+
                                        "%3d| 0->10.0, 1->15.0, 2->5.0, 3->4.0\n"+
                                        "%3d| 1->2.0, 2->3.0, 3->4.0\n"+
                                        "%3d| 2->5.0\n"+
                                        "%3d| 0->1.0, 1->2.0, 2->6.0, 3->4.0\n"+
                                        "%3d| 0->5.0, 1->2.0, 2->2.0, 3->2.0",
                                "row",0,1,2,3,4
                        )
                ,
                sparseMatrix.toString().equals(
                        String.format("%3s| index->value\n"+
                                        "%3d| 0->10.0, 1->15.0, 2->5.0, 3->4.0\n"+
                                        "%3d| 1->2.0, 2->3.0, 3->4.0\n"+
                                        "%3d| 2->5.0\n"+
                                        "%3d| 0->1.0, 1->2.0, 2->6.0, 3->4.0\n"+
                                        "%3d| 0->5.0, 1->2.0, 2->2.0, 3->2.0",
                                "row",0,1,2,3,4
                        )
                )
        );
    }

}