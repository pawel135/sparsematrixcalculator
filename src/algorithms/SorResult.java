package algorithms;

import vector.SparseVector;

public class SorResult {
    SparseVector result;
    double meanError;
    int numberOfIterations;
    double solvingTime;

    public SorResult() {

    }
    public SorResult(SparseVector result, double meanError, int numberOfIterations, double solvingTime){
        this.result = result;
        this.meanError = meanError;
        this.numberOfIterations = numberOfIterations;
        this.solvingTime = solvingTime;
    }
    public double getMeanError() {
        return meanError;
    }

    public void setMeanError(double meanError) {
        this.meanError = meanError;
    }

    public int getNumberOfIterations() {
        return numberOfIterations;
    }

    public void setNumberOfIterations(int numberOfIterations) {
        this.numberOfIterations = numberOfIterations;
    }

    public double getSolvingTime() {
        return solvingTime;
    }

    public void setSolvingTime(double solvingTime) {
        this.solvingTime = solvingTime;
    }

    public SparseVector getResult() {
        return result;
    }
    public void setResult(SparseVector result) {
        this.result = result;
    }
}
