package algorithms;

import matrix.SparseMatrix;
import vector.IndexNotInVectorException;
import vector.InvalidVectorSizeException;
import vector.SparseVector;
import vector.ZeroInsertingException;

public class SorSolver {
    public static SorResult solveSor(SparseMatrix A, SparseVector b, double errorTolerance, int maxNumberOfIterations ) throws InvalidVectorSizeException, IndeterminateSystemException, ZeroOnDiagonalException{
        // TODO: count time

        double omega = calculateBestOmega(A);
        double meanError = errorTolerance;
        int numberOfIterations = 0;
        double solvingTime = 0;
        int numberOfRows = A.getNumberOfRows();
        int numberOfColumns = A.getNumberOfColumns();

        SparseVector result = new SparseVector(numberOfColumns);
        SparseVector resultOld;

        if ( numberOfColumns != b.getSize() ){
            throw new InvalidVectorSizeException();
        }
        if ( numberOfColumns == 1) {
            try{
                result.setSize(1);
                result.insert(0, b.get(0) / A.getMatrixRepresentation().get(0).get(0) );
            } catch (IndexNotInVectorException e){
                // OK, value might be 0
            } catch (ZeroInsertingException e){
                // OK, nothing will be inserted
            } catch (ArithmeticException e){
                throw new IndeterminateSystemException();
            }
            return new SorResult(result, 0.0, 1, solvingTime);
        }
        double value = 0.;
        while ( meanError >= errorTolerance && numberOfIterations < maxNumberOfIterations ){
            resultOld = result;
            System.out.println("iteration: " + numberOfIterations);
            System.out.println("resultOld: " + resultOld.toString());

            for ( int i = 0; i < numberOfRows; i++) {
                try {
                    value = b.get(i);
                } catch (IndexNotInVectorException e) {
                    //OK, value might be zero
                }
                System.out.println( new SparseVector(A,i,0,i-1) );
                System.out.println( new SparseVector(result,0,i-1) );
                System.out.println( new SparseVector(A,i,0,i-1).multiplyGetScalar(new SparseVector(result,0,i-1)) );
                value -= new SparseVector(A,i,0,i-1).multiplyGetScalar(new SparseVector(result,0,i-1)) +
                        new SparseVector(A,i,i+1,numberOfColumns-1).multiplyGetScalar( new SparseVector(resultOld, i+1, numberOfColumns-1));
                try {
                    value /= A.getMatrixRepresentation().get(i).get(i);
                } catch (IndexNotInVectorException e) {
                    throw new ZeroOnDiagonalException();
                }
                double oldValue;
                try{
                    oldValue = resultOld.get(i);
                } catch (IndexNotInVectorException e){
                    oldValue = 0.;
                }
                try {
                    result.insert(i, (1 - omega) * oldValue + omega * value);
                } catch (ZeroInsertingException e){
                    //OK, as the result might be 0
                }
                try {
                    System.out.println("result(" + i + ") = " + result.get(i));
                } catch (IndexNotInVectorException e){
                    System.out.println("result(" + i + ") = " + 0.0 +" (not inserted)");

                }
            }
            meanError = calculateMeanError(A,result,b); // throws InvalidVectorSizeException
            System.out.println("error: "+  meanError );
            numberOfIterations++;
        }


        // TODO: solvingTime = ;
        SorResult sorResult = new SorResult( result, meanError, numberOfIterations, solvingTime);

        return sorResult;
    }

    public static double calculateMeanError(SparseMatrix A, SparseVector x, SparseVector b) throws InvalidVectorSizeException{
        return A.multiplyVector(x).subtract(b).abs().average();
    }

    public static double calculateBestOmega(SparseMatrix A){
        // ro = spectralRadius(A) // biggest eigenvalue, use power method
        // omega = 2/(1+Math.sqrt(1-Math.pow(ro,2)))

        return 1.;
    }
}
