package algorithms;

import matrix.SparseMatrix;
import org.junit.Before;
import org.junit.Test;
import vector.InvalidVectorSizeException;
import vector.SparseVector;
import vector.ZeroInsertingException;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Vector;

import static org.junit.Assert.*;

public class SorSolverTest {
    private SparseMatrix A;
    private SparseVector b, x;
    private double epsilon = 1e-9;
    private final static double ERROR_TOLERANCE = 1e-9;
    private final static int MAX_ITERATIONS = 1000;
    @Before
    public void setUp() throws Exception {
        int rows = 3;
        int cols = 3;
        A = createMatrix();
        b = new SparseVector(cols);
        x = new SparseVector(cols);
        try{
            b.insert(0,-1.);
            b.insert(1,7.);
            b.insert(2,-7.);

            x.insert(0,0.9);
            x.insert(1,2.1);
            x.insert(2,-2.1);
        } catch (ZeroInsertingException e){
            System.err.println("Improper implementation of insert() method or setUp in SorSolverTest");
        }
    }

    @Test
    public void testSolveSor() throws Exception {
        try{
            System.out.println( SorSolver.solveSor(A,b,ERROR_TOLERANCE,MAX_ITERATIONS).getResult().toString() );
        } catch (InvalidVectorSizeException e){
            System.err.println("SorSolverTest: wrong dimensions of A,x or b ");
        } catch (IndeterminateSystemException e){
            System.err.println("SorSolverTest: set is not solvable with SOR method");
        } catch ( ZeroOnDiagonalException e){
            System.err.println("SorSolverTest: set is not solvable with SOR method as there is 0 on matrix A diagonal");
        }

    }

    @Test
    public void testCalculateMeanError() throws Exception {
        try{
            assertEquals(0.5, SorSolver.calculateMeanError(A,x,b), epsilon );
        } catch (InvalidVectorSizeException e){
            System.err.println("Wrong sizes of A,x or b in test for calculateMeanError()");
        }
    }

    @Test
    public void testCalculateBestOmega() throws Exception {

    }
    private SparseMatrix createMatrix(){
        SparseMatrix sparseMatrix = null;
        Path resultPath = Paths.get("testMatrix");
        int rows = 3;
        int cols = 3;
        Vector<SparseVector> vectorOfSparseVectors = new Vector<>(rows);

        try {
            SparseVector matrixRow = new SparseVector(cols);
            matrixRow.insert(0, 3.);
            matrixRow.insert(1, -1.);
            matrixRow.insert(2, 1.);
            vectorOfSparseVectors.insertElementAt(matrixRow, 0);

            matrixRow = new SparseVector(cols);
            matrixRow.insert(0, -1.);
            matrixRow.insert(1, 3.);
            matrixRow.insert(2, -1.);
            vectorOfSparseVectors.insertElementAt(matrixRow, 1);

            matrixRow = new SparseVector(cols);
            matrixRow.insert(0, 1.);
            matrixRow.insert(1, -1.);
            matrixRow.insert(2, 3.);
            vectorOfSparseVectors.insertElementAt(matrixRow, 2);

        } catch( ZeroInsertingException e){
            System.err.println("Improper implementation of sparseVector insert() or try to insert 0 in sparseMatrix tests setUp()");
        }
        try {
            sparseMatrix = new SparseMatrix(vectorOfSparseVectors, rows, cols, resultPath);
        } catch(IOException e){
            e.printStackTrace();

        }
        return sparseMatrix;
    }
}