package algorithms;

import vector.UnsortedVectorException;

import java.util.Vector;

public class Sorter {
    // sorted only for additional sorting of indexes
    public static Vector<Integer> insertSortedInto(Integer value, Vector<Integer> indexes) throws UnsortedVectorException {
        // if (value == null ){ throw new NullInsertingException() };
        if (indexes == null){
            indexes = new Vector<>();
            indexes.add(value);
            return indexes;
        }
        boolean isAscending = false;
        try{
            isAscending = isAscending(indexes);
        } catch ( UnsortedVectorException e){
            throw e;
        }
        if ( !isAscending ){ // Descending
            int i = 0;
            while( i < indexes.size() &&  value < indexes.get(i) ){
                i++;
            }
            if ( i >= indexes.size() ){
                indexes.add(value);
            } else {
                indexes.insertElementAt(value,i);
            }
            return indexes;
        } else{ // ascending or constant order -> ascending result
            int i = 0;
            while( i < indexes.size() &&  value >= indexes.get(i)) {
                i++;
            }
            if ( i >= indexes.size() ){
                indexes.add(value);
            } else {
                indexes.insertElementAt(value,i);
            }
            return indexes;
        }

    }
    //TODO: move to some class e.g. Sorter
    public static boolean isAscending(Vector<Integer> values) throws UnsortedVectorException{
        if ( values == null || values.size() < 1){
            return false;
        }
        if ( values.size() < 2){
            return true;
        }
        boolean isAsc = false;
        boolean isDesc = false;
        for ( int i = 1; i < values.size(); i++){
            if ( values.get(i-1) > values.get(i) ){ // if DESC
                if ( isAsc ){
                    throw new UnsortedVectorException();
                } else {
                    isDesc = true;
                }
            } else 	if ( values.get(i-1) < values.get(i) ){ // if ASC
                if ( isDesc ){
                    throw new UnsortedVectorException();
                } else {
                    isAsc = true;
                }
            }
            else{ // if CONST

            }
        }
        return (isAsc == isDesc) ? true : isAsc; // 2x false means const and it's treated as Asc
    }

}
