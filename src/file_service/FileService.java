package file_service;

import vector.SparseVector;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Vector;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FileService {
    private Path path;
    private static final int MAX_FILE_OF_THE_SAME_NAME = 100;
    private static final String commentarySign = "#";

    public FileService(Path path) throws IOException {
        this.path = path;
    }


    public String getFirstLine() throws IOException {
        FileReader fr = new FileReader(path.toString());
        BufferedReader br = new BufferedReader(fr);
        try {
            try {
                return br.readLine();
            } catch (IOException e) {
                throw e;
            }
        } catch (FileNotFoundException e2) {
            throw e2;
        }
    }

    public String[] getFileLines() throws IOException {
        FileReader fr = new FileReader(path.toString());
        BufferedReader br = new BufferedReader(fr);
        String[] result = br.lines().filter(s->!s.matches("^\\s*$"))
            .filter(s->( !s.matches("^\\s*\\#.*$") ))
                .toArray(String[]::new);
        br.close();
        return result;
    }

    public void writeLines(String[] lines) throws IOException {
        PrintWriter pw = new PrintWriter(path.toFile());
        for (String line : lines) {
            pw.append(line);
        }
        pw.flush();
        pw.close();
    }

    public Path saveFileOverwrite(Vector<SparseVector> content, int numberOfRows, int numberOfColumns) throws FileAlreadyExistsException, IOException {
        return saveFile(content, numberOfRows,numberOfColumns, true);

    }
    public Path saveFileDontOverwrite(Vector<SparseVector> content, int numberOfRows, int numberOfColumns) throws FileAlreadyExistsException, IOException {
        return saveFile(content, numberOfRows,numberOfColumns, false);

    }

    private Path saveFile(Vector<SparseVector> content, int numberOfRows, int numberOfColumns, boolean overwrite) throws FileAlreadyExistsException, IOException {
        int i = 0;
        String name;
        String extension;
        String pathResultName = path.toString();
        String pathTmp = pathResultName;

        if ( !overwrite ) {
            while (Files.exists(Paths.get(pathTmp))) {
                int dotIndex = pathResultName.lastIndexOf('.');
                name = pathResultName.substring(0, dotIndex);
                extension = pathResultName.substring(dotIndex);
                i++;
                if (i > MAX_FILE_OF_THE_SAME_NAME) {
                    throw new FileAlreadyExistsException(pathResultName);
                }
                pathTmp = name + "(" + i + ")" + extension;
            }
            pathResultName = pathTmp;

            try {
                System.out.println(pathResultName);
                this.path = Files.createFile(Paths.get(pathResultName));
            } catch (IOException e) {
                throw e;
            }
        } else {
           if (!Files.exists(path)){
               this.path = Files.createFile(path);
           }
        }
        PrintWriter pw = new PrintWriter(path.toFile());
        pw.write(Arrays.stream(FileParser.makeStringLinesFromContent(content, numberOfRows, numberOfColumns)).collect(Collectors.joining("\n")));
        pw.flush();
        pw.close();
        return path;
    }


    public static String makeFilename(String s1, String s2, String joiner) {
        String result, extension = "";
        int dotIndex = s1.lastIndexOf('.');
        if ( dotIndex > 0) {
            extension = s1.substring(dotIndex);
            s1 = s1.substring(0, dotIndex);
        }
        dotIndex = s2.lastIndexOf('.');
        if ( dotIndex > 0) {
            s2 = s2.substring(0, dotIndex);
        }
        return s1 + joiner + s2 + extension;
    }

    public static void main (String [] args){
        try {
            FileService fs = new FileService( Paths.get("matA.txt") );
            try{
                System.out.println( fs.getFirstLine() );
            } catch (IOException e){
                System.err.println("Error while reading data");
            }

            System.out.println( Arrays.stream(fs.getFileLines()).collect(Collectors.joining("\n")) );


        } catch(FileAlreadyExistsException e){
            System.err.println("Result file already exists");
        } catch (IOException e){
            System.err.println("Cannot access specified files");
        }
    }

//    public getArrayListOfVectorsFromInput() {
//       return
//   }

}
