package file_service;

import vector.IndexNotInVectorException;
import vector.SparseVector;
import vector.ZeroInsertingException;

import java.io.IOException;
import java.util.Vector;

public class FileParser {

    public static Vector<SparseVector> parseFileContent(String[] lines) throws ZeroInsertingException, InvalidFileFormattingException, IOException {
        Vector<SparseVector> result = new Vector<>();
        SparseVector sparseVector;
        int numberOfLines = lines.length;
        int numberOfRows = getNumberOfRows(lines[0]);
        int numberOfColumns = getNumberOfColumns(lines[0]);
        int lineIndex = 1;

        for (int i = 0; i < numberOfRows; i++) {
            sparseVector = new SparseVector();
            result.add(sparseVector);
        }

        while (lineIndex < numberOfLines) {

            int row = getRowFromLine(lines[lineIndex]);
            if (row >= numberOfRows) {
                throw new InvalidFileFormattingException();
            }


            int col = getColumnFromLine(lines[lineIndex]);
            if (col < 0 || col >= numberOfColumns) {
                throw new InvalidFileFormattingException();
            }
            double val = getValueFromLine(lines[lineIndex]);
            try {
                result.get(row).insert(col, val);
            } catch (ZeroInsertingException e2) {
                throw e2;
            }
            lineIndex++;
        }

        return result;
    }


    public static int getNumberOfRows(String firstLine) throws InvalidFileFormattingException {
        if (firstLine == null) {
            throw new InvalidFileFormattingException();
        }
        String[] parts = firstLine.split("\\s+");
        if (parts.length != 2) {
            throw new InvalidFileFormattingException();
        } else {
            return Integer.valueOf(parts[0]);
        }
    }

    public static int getNumberOfColumns(String firstLine) throws InvalidFileFormattingException {
        if (firstLine == null) {
            throw new InvalidFileFormattingException();
        }
        String[] parts = firstLine.split("\\s+");
        if (parts.length != 2) {
            throw new InvalidFileFormattingException();
        } else {
            return Integer.valueOf(parts[1]);
        }
    }


    private static int getRowFromLine(String line) throws NumberFormatException {
        try{
            return Integer.parseInt( line.split("\\s+")[0] );
        } catch(NumberFormatException e){
            throw e;
        }
    }

    private static int getColumnFromLine(String line) throws NumberFormatException {
        try{
            return Integer.parseInt( line.split("\\s+")[1] );
        } catch(NumberFormatException e){
            throw e;
        }
    }


    private static double getValueFromLine(String line) throws NumberFormatException{
        try{
            return Double.parseDouble( line.split("\\s+")[2] );
        } catch(NumberFormatException e){
            throw e;
        }
    }


    public static String [] makeStringLinesFromContent(Vector<SparseVector> content, int numberOfRows, int numberOfColumns){
        int fileLine = 0;
        int numberOfFileLines = (int)(1 + content.stream().filter(c -> c.getLength() > 0).mapToInt(c->c.getIndexes().size()).sum());

        String [] result = new String[numberOfFileLines];
        result[fileLine++] = String.valueOf(numberOfRows) + " " + String.valueOf(numberOfColumns);
        for (int row = 0; row < numberOfRows; row++){
            if (content.get(row).getLength() > 0 ){
                Vector<Integer> indexes = content.get(row).getIndexes();
                for ( int col : indexes){
                    StringBuilder sb = new StringBuilder();
                    sb.append(row + " " + col + " ");
                    try{
                        sb.append( content.get(row).get(col) );
                    } catch (IndexNotInVectorException e){
                        System.err.println("FileParser.makeStringLinesFromContent: IndexesMap improperly created");
                    }
                    result[fileLine++] = sb.toString();
                }
                //sb.append( content.get(row).getIndexes().toString().replaceAll("[^(\\d,)]","").replaceAll(","," ")  );
            }
        }
        return result;
    }

}
