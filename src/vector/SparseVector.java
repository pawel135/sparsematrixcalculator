package vector;

import algorithms.Sorter;
import matrix.InvalidMatrixSizeException;
import matrix.SparseMatrix;
import tree.IndexNotFoundInTreeException;
import tree.Tree;

import java.util.Arrays;
import java.util.Vector;

public class SparseVector {
    public static final double EPSILON = 1e-9;
    private Tree tree = new Tree();
    private Vector<Integer> indexes = new Vector();
    private int lastIndex = 0;
    private int size = 16;

    public int getLength() {
        return indexes.size();
    }

    public int getSize() {
        return size;
    }

    public SparseVector(){
    }

    public SparseVector(int initialSize){
        size = initialSize;
    }

    public SparseVector(SparseVector sparseVector, int rangeStart, int rangeEnd) {

        if ( rangeStart > rangeEnd || rangeStart < 0 || rangeEnd >= sparseVector.getSize() ){
            this.setSize(0);
            return;
        }
        this.setSize(rangeEnd - rangeStart + 1);
        for ( int i : sparseVector.getIndexes() ){
            if ( i > rangeEnd ){
                break;
            }
            if ( i >= rangeStart ){
                try {
                    this.insert(i - rangeStart, sparseVector.get(i));
                } catch( IndexNotInVectorException e){
                    System.err.println("Improper implementation of either creating indexes or get() method");
                } catch (ZeroInsertingException e){
                    System.err.println("Improper implementation of creating indexes or get() or insert() method");
                }
            }
        }
    }

    public SparseVector(SparseMatrix sparseMatrix, int row, int colRangeStart, int colRangeEnd){
        this(sparseMatrix.getMatrixRepresentation().get(row), colRangeStart, colRangeEnd );
    }

    public void setSize(int value){
        this.size = value;
    }

    public void insert(int index, double value) throws ZeroInsertingException, ArrayIndexOutOfBoundsException {
        if ( index >= size ){
            throw new ArrayIndexOutOfBoundsException();
        }
        if (Math.abs(value) < EPSILON) {
            throw new ZeroInsertingException();
        }


        if ( !contains(index) ) {
            try {
                indexes = Sorter.insertSortedInto(index, indexes);
            } catch (UnsortedVectorException e){
                System.err.println("Bad implementation of vector sorting while inserting");
            }
            //indexes.add(index);
            lastIndex = Math.max( index, lastIndex);
        }
        tree.insert(index, value);
    }


    public void insert(double value) throws ZeroInsertingException, ArrayIndexOutOfBoundsException {
        if ( lastIndex+1 >= size ){
            throw new ArrayIndexOutOfBoundsException();
        }
        if (Math.abs(value) < EPSILON) {
            throw new ZeroInsertingException();
        }
        tree.insert(lastIndex, value);
        try {
            indexes = Sorter.insertSortedInto(lastIndex, indexes);
        } catch (UnsortedVectorException e){
            System.err.println("Bad implementation of vector sorting while inserting");
        }
        //indexes.add(lastIndex);
        lastIndex++;
    }

    public double get(int index) throws IndexNotInVectorException, ArrayIndexOutOfBoundsException {
        if (index < 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        try {
            return tree.getValueOnIndex(index);
        } catch (IndexNotFoundInTreeException e) {
            throw new IndexNotInVectorException();
        }
    }

    public boolean contains(int index) {
        try {
            get(index);
        } catch (IndexNotInVectorException|ArrayIndexOutOfBoundsException e){
            return false;
        }
        return true;
    }

    public String toString(){
        return Arrays.toString(tree.getAll());
    }

    public void remove(int index) throws IndexNotInVectorException, ArrayIndexOutOfBoundsException{
        if ( index >= size ){
            throw new ArrayIndexOutOfBoundsException();
        }
        try{
            tree.delete(index);
            indexes.remove(index);
        } catch ( IndexNotFoundInTreeException e){
            throw new IndexNotInVectorException();
        }

    }

    public Vector<Integer> getIndexes(){
        return indexes;
    }



    public double sum(){
        return Arrays.stream( tree.getAll() ).sum();
    }

    /*public double sum(){
        return this.getIndexes().parallelStream().mapToDouble(i-> {
            try{
                return get(i);
            } catch (IndexNotInVectorException e){
                return 0;
            }
        } )
                .reduce( (sum,v) -> sum+v ).getAsDouble();
    }*/

    public SparseVector abs(){
        for ( int index : indexes){
            try{
                tree.insert(index, Math.abs(tree.getValueOnIndex(index)));
            } catch (IndexNotFoundInTreeException e){
                throw new ArrayIndexOutOfBoundsException();
            }
        }
        return this;
    }

    public SparseVector add(double value){
        SparseVector result = new SparseVector();
        result.setSize(this.getSize());
        for ( int i = 0; i < this.getSize(); i++ ){
            double current;
            try{
                current = this.get(i);
            } catch ( IndexNotInVectorException e ){
                current = 0.;
            }
            try {
                result.insert(i,current + value);
            } catch (ZeroInsertingException e){
                // OK, nothing will be inserted
            }

        }

        return result;
    }


    public SparseVector addToNonZero(double value) {
        SparseVector result = new SparseVector();
        result.setSize( this.getSize() );
        indexes.stream().forEach(i-> {
            try {
                result.insert(i, get(i) + value);
            } catch (ZeroInsertingException e) {
                // OK, it happens when adding
            } catch (IndexNotInVectorException e){
                System.err.println("Improper addToNonZero() implementation");
            }
        });
        return result;
    }

    public SparseVector add(SparseVector other){
        SparseVector result = new SparseVector();
        result.setSize(Math.max(this.getSize(),other.getSize()));
        for ( int i = 0; i < result.getSize(); i++){
            double current = 0.;
            try{
                current += this.get(i);
            } catch (IndexNotInVectorException|ArrayIndexOutOfBoundsException e){
            }
            try{
                current += other.get(i);
            } catch(IndexNotInVectorException|ArrayIndexOutOfBoundsException e){
            }
            try{
                result.insert(i,current);
            } catch(ZeroInsertingException e){
                // OK,
            }
        }

        return result;
    }

    public SparseVector subtract(SparseVector other){
        return this.add( other.multiply(-1) );
    }

    public SparseVector subtract(double value){
        return this.add(-value);
    }


    public double average(){
        return this.sum()/this.getSize();
    }

    public SparseVector multiply(double value){
        SparseVector result = new SparseVector();
        result.setSize(this.getSize());
        for ( int i : indexes ){
            try{
                result.insert( i, this.get(i)*value );
            } catch (IndexNotInVectorException|ZeroInsertingException e){
                // Zero... - OK as the value that we multiply by can be 0
            }
        }
        return result;
    }

    public SparseVector multiplyMatrix(SparseMatrix other) throws InvalidMatrixSizeException{
        if ( this.getSize() !=  other.getNumberOfRows() ){
            throw new InvalidMatrixSizeException();
        }
        int rows = other.getNumberOfRows();
        int cols = other.getNumberOfColumns();

        SparseVector result = new SparseVector();
        result.setSize(cols);
        double value;

        for ( int i = 0; i < cols; i++){
            try{
                value = this.multiplyGetScalar( other.getColumn(i) );
                try{
                    result.insert(i,value);
                } catch( ZeroInsertingException e){
                    // OK as small values may be multiplied, it simply will not be inserted
                }

            } catch(InvalidVectorSizeException e){
                throw new InvalidMatrixSizeException();
            }
        }

        return result;
    }

    public double multiplyGetScalar(SparseVector other) throws InvalidVectorSizeException, ArrayIndexOutOfBoundsException{
        //return: scalar. Check if size == other.size
        if (this.getSize() != other.getSize()){
            throw new InvalidVectorSizeException();
        }
        double result = 0.;
        for ( int i : indexes ){
            try{
                result += this.get(i) * other.get(i) ;
            } catch (IndexNotInVectorException e){
                //OK
            } catch (ArrayIndexOutOfBoundsException e){
                throw e;
            }
        }
        return result;
    }


    public SparseVector tranpose(){ //No need to do anything, it's just a concept
        return this;
    }







}

