package vector;

import matrix.InvalidMatrixSizeException;
import matrix.SparseMatrix;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Vector;

import static org.junit.Assert.*;

public class SparseVectorTest {
    SparseVector sparseVector;
    double epsilon;
    @Before
    public void setUp(){
        sparseVector = new SparseVector(100);
        epsilon = 1e-9;
        try {
            sparseVector.insert(12.2);
            sparseVector.insert(412.12);
            sparseVector.insert(1.23);
            sparseVector.insert(12.2);
        } catch (ZeroInsertingException e){
            fail();
        }
    }


    @Test
    public void testConstructorVector(){
        SparseVector result = new SparseVector(sparseVector,2,8);
        try{
            assertEquals(7,result.getSize());
            assertEquals( 1.23, result.get(0), epsilon);
            assertEquals( 12.2, result.get(1), epsilon);
        } catch (IndexNotInVectorException e){
            System.err.println("Improper vector get method");
        }
        try{
            for ( int i = 2; i < result.getSize() ; i++) {
                result.get(i);
            }
            fail();
        } catch ( IndexNotInVectorException e){
            //OK
        } catch (ArrayIndexOutOfBoundsException e){
            fail();
        }

    }

    @Test
    public void testConstructorMatrix(){
        SparseVector result = new SparseVector(createMatrix(),0,1,2);
        try{
            assertEquals(2,result.getSize());
            assertEquals( 15., result.get(0), epsilon);
            assertEquals( 5.0, result.get(1), epsilon);
        } catch (IndexNotInVectorException e){
            System.err.println("Improper vector get method");
        }
    }

    @Test
    public void testInsert1() {
        try{
            sparseVector.insert(65,762.2);
        } catch (ZeroInsertingException e){
            fail();
        }
        assertEquals(5, sparseVector.getLength());
        try{
            assertEquals(762.2, sparseVector.get(65),1e-9);
        } catch (IndexNotInVectorException e){
            fail();
        }
    }

    @Test
    public void testInsert2(){
        assertEquals(4, sparseVector.getLength());
        try{
            sparseVector.insert(674.21);
        } catch (ZeroInsertingException e){
            fail();
        }
        assertEquals(5, sparseVector.getLength());
        try{
            assertEquals(674.21, sparseVector.get(4),epsilon);
        } catch (IndexNotInVectorException e){
            fail();
        }
    }

    @Test(expected=ArrayIndexOutOfBoundsException.class)
    public void testInsert3() {
        try{
            sparseVector.insert(100,1762.2);
        } catch (ZeroInsertingException e){
            fail();
        }
    }

    @Test
    public void testGet(){
        try{
            assertTrue( Math.abs( 12.2- sparseVector.get(0) ) < epsilon);
            assertTrue( Math.abs( 412.12- sparseVector.get(1) ) < epsilon);
            assertTrue( Math.abs( 1.23- sparseVector.get(2) ) < epsilon);
            assertTrue( Math.abs( 12.2- sparseVector.get(3) ) < epsilon);
        } catch (IndexNotInVectorException e){
           fail();
        }
    }


    @Test(expected=ArrayIndexOutOfBoundsException.class)
    public void testGet2(){
        try{
            sparseVector.get(100);
        } catch (IndexNotInVectorException e){
            fail();
        }
    }

    @Test(expected=IndexNotInVectorException.class)
    public void testGet3() throws IndexNotInVectorException{
        try{
            sparseVector.get(45);
        } catch (ArrayIndexOutOfBoundsException e){
            fail();
        } catch (IndexNotInVectorException e){
            throw e;
        }
    }


    @Test
    public void testSetSize(){
        sparseVector.setSize(120);
        try{
            sparseVector.insert(111,23.4);
        } catch (ZeroInsertingException e){
            fail();
        }
        try{
            sparseVector.get(111);
        } catch (ArrayIndexOutOfBoundsException|IndexNotInVectorException e){
            fail();
        }
    }

    @Test
    public void testSetSize2(){
        sparseVector.setSize(120);
        try{
            sparseVector.get(111);
        } catch (ArrayIndexOutOfBoundsException e){
            fail();
        } catch (IndexNotInVectorException e){
            //OK
        }
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testGetOutOfBounds(){
        try{
            sparseVector.get(100);
        } catch (IndexNotInVectorException e){
            fail();
        }
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testGetNegativeIndex(){
        try{
            sparseVector.get(-1);
        } catch (IndexNotInVectorException e){
            fail();
        }
    }

    @Test
    public void testToString(){
        assertEquals("[12.2, 412.12, 1.23, 12.2]", sparseVector.toString());
    }

    @Test
    public void testInsertCloseToZero(){
        try{
            sparseVector.insert(epsilon);
        } catch(ZeroInsertingException e){
            fail();
        }
    }

    @Test(expected = ZeroInsertingException.class)
    public void testInsertZero() throws ZeroInsertingException{
        try {
            sparseVector.insert(0);
        } catch(ZeroInsertingException e){
            throw e;
        }
    }

    @Test
    public void testPrint(){
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        System.out.println(sparseVector.toString());
        assertEquals("[12.2, 412.12, 1.23, 12.2]\n",outContent.toString());
    }


    @Test
    public void testSum(){
        assertEquals(437.75, sparseVector.sum(), epsilon);
    }

    @Test
    public void testAverage(){
        assertEquals(437.75/sparseVector.getSize() , sparseVector.average(), epsilon);
    }

    @Test
    public void testAbs(){
        try {
            sparseVector.insert(-9.2);
            sparseVector.insert(-3.3);
        } catch (ZeroInsertingException e){
            System.out.println("Improper abs() implementation");
        }
        sparseVector.abs();
        try {
            assertEquals(12.2, sparseVector.get(0), epsilon);
            assertEquals(412.12, sparseVector.get(1), epsilon);
            assertEquals(1.23, sparseVector.get(2), epsilon);
            assertEquals(12.2, sparseVector.get(3), epsilon);
            assertEquals(9.2, sparseVector.get(4), epsilon);
            assertEquals(3.3, sparseVector.get(5), epsilon);
        } catch(IndexNotInVectorException e){
            fail();
        }
    }


    @Test(expected=IndexNotInVectorException.class)
    public void testIndexNotInVectorException() throws IndexNotInVectorException{
        try {
            sparseVector.get(0);
            sparseVector.get(2);
            sparseVector.get(4);
            sparseVector.get(24);
            assertEquals(400.0, sparseVector.get(1), epsilon);
            assertEquals(-10.97, sparseVector.get(2), epsilon);
            assertEquals(-12.2, sparseVector.get(3), epsilon);
        } catch (IndexNotInVectorException e){
            throw e;
        }
    }


    @Test(expected = IndexNotInVectorException.class)
    public void testRemove() throws IndexNotInVectorException{
        try{
            sparseVector.get(0);
        } catch (IndexNotInVectorException e){
            fail();
        }

        sparseVector.remove(0);
        sparseVector.get(0);
    }


    @Test
    public void testAddValue(){
        double value = -12.12;
        try {
            sparseVector.insert(22, 13.12);
        } catch( ZeroInsertingException e){
        }
        sparseVector = sparseVector.add(value);
        try {
            assertEquals(400.0, sparseVector.get(1), epsilon);
            assertEquals(-10.89, sparseVector.get(2), epsilon);
            assertEquals(1., sparseVector.get(22), epsilon);
        } catch (IndexNotInVectorException e){
            fail();
        }
    }

    @Test
    public void testAddValueToNonZero() {
        double value = -12.12;
        try {
            sparseVector.insert(22, 13.12);
        } catch( ZeroInsertingException e){
        }
        sparseVector = sparseVector.addToNonZero(value);

        try {
            assertEquals(0.08, sparseVector.get(0), epsilon);
            assertEquals(400.0, sparseVector.get(1), epsilon);
            assertEquals(-10.89, sparseVector.get(2), epsilon);
            assertEquals(1., sparseVector.get(22), epsilon);
        } catch (IndexNotInVectorException e){
            fail();
        }
        try {
            assertNotEquals(-12.12, sparseVector.get(23), epsilon);
            fail();
        } catch (IndexNotInVectorException e) {
            //OK
        }


    }

    @Test
    public void testAddVector(){
        SparseVector other = new SparseVector();
        SparseVector result;
        other.setSize(120);
        epsilon = 1e-5;
        try {
            other.insert(0.8); //index 0
            other.insert(-412.12); //index 1

            other.insert(3,-12.2);
            other.insert(100,10.);
        } catch (ZeroInsertingException e){

        }
        result = sparseVector.add(other);
        try{
            assertEquals(13.0, result.get(0),epsilon);
        } catch (IndexNotInVectorException|ArrayIndexOutOfBoundsException e){
            fail();
        }
        try{
            result.get(1);
            fail();
        } catch ( IndexNotInVectorException e){
            //OK
        } catch (ArrayIndexOutOfBoundsException e){
            fail();
        }
        try{
            assertEquals(1.23, result.get(2),epsilon);
        } catch ( IndexNotInVectorException|ArrayIndexOutOfBoundsException e){
            fail();
        }
        try{
            System.err.println( ">>"+result.get(3) );
            fail();
        } catch (IndexNotInVectorException e){
            //OK
        } catch (ArrayIndexOutOfBoundsException e){
            fail();
        }
        try {
            assertEquals(10.0, result.get(100), epsilon);
        } catch (IndexNotInVectorException|ArrayIndexOutOfBoundsException e){
            fail();
        }
    }


    @Test
    public void testSubtractValue(){
        SparseVector result = sparseVector.subtract( 20 );
        assertTrue( result.getSize() == sparseVector.getSize() );
        try {
            assertEquals(-7.8, result.get(0), epsilon);
            assertEquals(392.12, result.get(1), epsilon);
            assertEquals(-18.77, result.get(2), epsilon);
            assertEquals(-7.8, result.get(3), epsilon);
            for ( int i = 4; i < result.getSize(); i++) {
                try {
                    assertEquals(-20, result.get(i), epsilon);
                } catch (IndexNotInVectorException e) {
                    fail();//OK, expected
                }
            }

        } catch( IndexNotInVectorException e){
            System.err.println("Wrong implementaion of get method or error in testSubstractVector implementation");
        }

    }

    @Test
    public void testSubtractVector(){
        SparseVector other = new SparseVector(sparseVector.getSize());
        try{
            other.insert(12.2);
            other.insert(12.12);
            other.insert(0.23);
            other.insert(4,5);
        } catch( ZeroInsertingException e){
            System.err.println("Improper insert implementation or error in testSubtractValue while inserting");
        }
        SparseVector result = sparseVector.subtract( other );
        assertTrue( result.getSize() == sparseVector.getSize() );

        try {
            assertEquals(400.0, result.get(1), epsilon);
            assertEquals(1.0, result.get(2), epsilon);
            assertEquals(12.2, result.get(3), epsilon);
            assertEquals(-5.0, result.get(4), epsilon);
            for ( int i = 5; i < result.getSize(); i++) {
                try {
                    result.get(i);
                    fail();
                } catch (IndexNotInVectorException e) {
                    //OK, expected
                }
            }
        } catch( IndexNotInVectorException e){
            System.err.println("Wrong implementaion of get method or error in testSubstractVector implementation");
        }
    }




    @Test
    public void testMultiply(){
        double value = 1.5;
        int oldSize = sparseVector.getSize();
        sparseVector = sparseVector.multiply( value );
        try {
            assertEquals( oldSize, sparseVector.getSize());
            assertEquals(18.3, sparseVector.get(0), epsilon);
            assertEquals(618.18, sparseVector.get(1), epsilon);
            assertEquals(1.845, sparseVector.get(2), epsilon);
            assertEquals(18.3, sparseVector.get(3), epsilon);
        } catch (IndexNotInVectorException e){
            fail();
        }
    }

    @Test(expected = IndexNotInVectorException.class)
    public void testMultiplyByZero() throws IndexNotInVectorException{
        sparseVector = sparseVector.multiply( 0. );
        try {
            sparseVector.get(0);
            fail();
        } catch (IndexNotInVectorException e){
            throw e;
        }
    }

    @Test
    public void testMultiplyGetScalar(){
        SparseVector other = new SparseVector();
        other.setSize( sparseVector.getSize() );
        try {
            other.insert(1, 2.);
            other.insert(3, 1.5);
        } catch (ZeroInsertingException e){

        }
        double result;
        try {
            result = sparseVector.multiplyGetScalar(other);
            assertEquals( 842.54, result, epsilon);
        } catch (InvalidVectorSizeException e){
            fail();
        }
    }

    @Test
    public void testMultiplyMatrix(){
        sparseVector = new SparseVector(3);
        SparseVector result;
        try {
            sparseVector.insert(1, 5.0);
            sparseVector.insert(2, 4.0);
        } catch( ZeroInsertingException e){
            e.printStackTrace();
            System.err.println("Improper inserting or testMultiplyMatrix implementation");
        }
        try {
            result = sparseVector.multiplyMatrix(createMatrix());
            assertTrue( result.toString().equals("[10.0, 35.0, 20.0]") );
        } catch (InvalidMatrixSizeException e){
            System.err.println("Improper testMultiplyMatrix implementation");
        }

    }





    private SparseMatrix createMatrix(){
        SparseMatrix sparseMatrix = null;
        Path resultPath = Paths.get("testMatrix");
        int rows = 3;
        int cols = 4;
        Vector<SparseVector> vectorOfSparseVectors = new Vector<>(rows);

        try {
            SparseVector matrixRow = new SparseVector(cols);
            matrixRow.insert(0, 10.0);
            matrixRow.insert(1, 15.0);
            matrixRow.insert(2, 5.0);
            matrixRow.insert(3, 4.0);
            vectorOfSparseVectors.insertElementAt(matrixRow, 0);

            matrixRow = new SparseVector(cols);
            matrixRow.insert(1, 2.0);
            matrixRow.insert(2, 3.0);
            matrixRow.insert(3, 4.0);
            vectorOfSparseVectors.insertElementAt(matrixRow, 1);

            matrixRow = new SparseVector(cols);
            matrixRow.insert(2, 5.0);
            vectorOfSparseVectors.insertElementAt(matrixRow, 2);

        } catch( ZeroInsertingException e){
            System.err.println("Improper implementation of sparseVector insert() or try to insert 0 in sparseMatrix tests setUp()");
        }
        try {
            sparseMatrix = new SparseMatrix(vectorOfSparseVectors, rows, cols, resultPath);
        } catch(IOException e){
            e.printStackTrace();

        }
        return sparseMatrix;
    }

}

