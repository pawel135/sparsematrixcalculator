package tree;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.Assert.*;


public class TreeTest {
    Tree tree;
    @Before
    public void setUp(){
        tree = new Tree();
        tree.insert(4,55.5);
        tree.insert(2,86.12);
    }


    @Test
    public void testInsert(){
        assertEquals(2,tree.getSize());
        tree.insert(5,12.2);
        assertEquals(3,tree.getSize());
        tree.insert(5,678.9);
        assertEquals(3,tree.getSize());
    }

    @Test
    public void testInsert1(){
        tree.insert(-1,12);
        assertEquals(3,tree.getSize());
    }

    @Test
    public void testGetValueOnIndex(){
        tree.insert(-1,12);
        try {
            assertEquals(55.5, tree.getValueOnIndex(4), 1e-9);
            assertEquals(86.12, tree.getValueOnIndex(2), 1e-9);
            assertEquals(12, tree.getValueOnIndex(-1), 1e-9);
        } catch(IndexNotFoundInTreeException e){
            fail();
        }
    }

    @Test(expected = IndexNotFoundInTreeException.class)
    public void testGetValueOnIndexException() throws IndexNotFoundInTreeException{
        try {
            assertEquals(55.5, tree.getValueOnIndex(100), 1e-9);
        } catch(IndexNotFoundInTreeException e){
            throw e;
        }
    }

    @Test
    public void testDump() {
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));


        TreeDump.dump(tree);
        assertEquals("index: 2, value: 86.12, depth: 2\n" +
                "index: 4, value: 55.5, depth: 1\n",outContent.toString());
    }

    @Test
    public void testGetAll(){
        tree.insert(45,123.456);
        String expectedStr = "[86.12, 55.5, 123.456]";
        assertEquals(expectedStr, Arrays.toString(tree.getAll()) );
    }

}