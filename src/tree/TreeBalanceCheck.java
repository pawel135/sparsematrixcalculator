package tree;

public class TreeBalanceCheck {


    public static boolean isHeightBalanced(Node root) {
        return maxLeafHeight(root) - minLeafHeight(root) <= 1;
    }

    public static int maxLeafHeight(Node node){
        if ( node == null ){
            return 0;
        }
        return Math.max(maxLeafHeight(node.left), maxLeafHeight(node.right) ) + 1;
    }

    public static int minLeafHeight(Node node){
        if ( node == null ){
            return 0;
        }
        return Math.min(minLeafHeight(node.left), minLeafHeight(node.right) ) + 1;
    }


    public static boolean isWeightBalanced(Node root){
        return getBalancedHeight(root) != -1;
    }

    public static int getBalancedHeight(Node node) {
        if (node == null){
            return 0;
        }

        int leftBalancedHeight = getBalancedHeight(node.left);
        int rightBalancedHeight = getBalancedHeight(node.right);

        if ((Math.abs(leftBalancedHeight - rightBalancedHeight) > 1) || (leftBalancedHeight == -1) || (rightBalancedHeight == -1)){
            return -1;
        }

        return Math.max(leftBalancedHeight, rightBalancedHeight) + 1;
    }
}
