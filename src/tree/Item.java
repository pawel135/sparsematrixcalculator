package tree;

class Item {
    int index;
    double value;

    Item(int index, double value){
        this.index = index;
        this.value = value;
    }
}
