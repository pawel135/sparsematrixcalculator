package tree;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Tree {
    public static final boolean RED = true;
    public static final boolean BLACK = false;
    private Node root;
//  private int size = 0;


    public Node getRoot(){
        return root;
    }

    public int getSize(){
        return Node.getSize(root);
    }

    public int getDepth(){
        int depth = 0;
        return getDepth(root,depth);
    }
    public int getDepth(Node node, int depth){
        if (node == null){
            return 0;
        }
        return Math.max( getDepth(node.left, depth), getDepth(node.right, depth) ) +1;
    }

    public double getValueOnIndex(int i) throws IndexNotFoundInTreeException{
        Node nodeFound = getNodeOnIndex(i,root);
        if ( null == nodeFound ){
            throw new IndexNotFoundInTreeException();
        }
        return nodeFound.item.value;
    }

    private Node getNodeOnIndex(int i, Node node){
        if ( null == node){
            return null;
        }
        if (node.item.index > i) {
            return getNodeOnIndex(i, node.left);
        } else if (node.item.index < i) {
            return getNodeOnIndex(i, node.right);
        } else {
            return node;
        }

    }

    public boolean containsIndex(int i){
        try{
            getValueOnIndex(i);
        } catch( IndexNotFoundInTreeException e){
            return false;
        }
        return true;
    }

    public boolean isEmpty(){
        return null == root;
    }


    public void insert(int i, double v){
        root = insert(i,v,root);
        root.setColor(BLACK);
    }

    private Node insert(int i, double v, Node node){
        if (node == null){
            return new Node(i,v,RED,1);
        }

        if ( i < node.item.index) {
            node.left = insert(i,v,node.left);
        } else if ( i > node.item.index ) {
            node.right = insert(i,v,node.right);
        } else {
            node.item.value = v;
        }

        if( Node.isRed(node.right) &&  !Node.isRed(node.left) ){
            node = rotateLeft(node);
        }
        if ( Node.isRed(node.left) && Node.isRed(node.left.left) ){
            node = rotateRight(node);
        }
        if ( Node.isRed(node.left) && Node.isRed(node.right) ){
            flipColors(node);
        }
        node.setSize(Node.getSize(node.left) + Node.getSize(node.right) + 1);

        return node;
    }

    // flip the colors of a node and its two children
    private void flipColors(Node node) {
        // h must have opposite color of its two children
        // assert (h != null) && (h.left != null) && (h.right != null);
        // assert (!isRed(h) &&  isRed(h.left) &&  isRed(h.right))
        //    || (isRed(h)  && !isRed(h.left) && !isRed(h.right));
        node.revertColor();
        node.left.revertColor();
        node.right.revertColor();
    }

    // make a left-leaning link lean to the right
    private Node rotateRight(Node h) {
        // assert (h != null) && isRed(h.left);
        Node x = h.left;
        h.left = x.right;
        x.right = h;
        x.setColor(x.right.getColor());
        x.right.setColor(RED);
        x.setSize(Node.getSize(h));
        h.setSize(Node.getSize(h.left) + Node.getSize(h.right) + 1);
        return x;
    }

    // make a right-leaning link lean to the left
    private Node rotateLeft(Node h) {
        // assert (h != null) && isRed(h.right);
        Node x = h.right;
        h.right = x.left;
        x.left = h;
        x.setColor(x.left.getColor());
        x.left.setColor(RED);
        x.setSize(Node.getSize(h));
        h.setSize(Node.getSize(h.left) + Node.getSize(h.right) + 1);
        return x;
    }

     //
     // Removes the smallest key and associated value from the symbol table.
     // @throws NoSuchElementException if the symbol table is empty
     //
    public void deleteMin() throws IndexNotFoundInTreeException{
        if (isEmpty()) throw new IndexNotFoundInTreeException();
        // if both children of root are black, set root to red
        if (!Node.isRed(root.left) && !Node.isRed(root.right))
            root.setColor(RED);

        root = deleteMin(root);
        if (!isEmpty())
            root.setColor(BLACK);
        // assert check();
    }

    // delete the key-value pair with the minimum key rooted at h
    private Node deleteMin(Node h) {
        if (h.left == null)
            return null;

        if (!Node.isRed(h.left) && !Node.isRed(h.left.left))
            h = moveRedLeft(h);

        h.left = deleteMin(h.left);
        return balance(h);
    }

    //
    //Removes the largest key and associated value from the symbol table.
    // @throws NoSuchElementException if the symbol table is empty
    //
    public void deleteMax() throws IndexNotFoundInTreeException {
        if (isEmpty()) throw new IndexNotFoundInTreeException();

        // if both children of root are black, set root to red
        if (!Node.isRed(root.left) && !Node.isRed(root.right))
            root.setColor(RED);

        root = deleteMax(root);
        if (!isEmpty()) root.setColor(BLACK);
        // assert check();
    }

    // delete the key-value pair with the maximum key rooted at h
    private Node deleteMax(Node h) {
        if (Node.isRed(h.left))
            h = rotateRight(h);

        if (h.right == null)
            return null;

        if (!Node.isRed(h.right) && !Node.isRed(h.right.left))
            h = moveRedRight(h);

        h.right = deleteMax(h.right);

        return balance(h);
    }



    //
     // Removes the specified key and its associated value from this symbol table
     // (if the key is in this symbol table).
     //
     // @param  key the key
     // @throws NullPointerException if {@code key} is {@code null}
     //

    public void delete(int index) throws IndexNotFoundInTreeException {
        if (!containsIndex(index)){
            throw new IndexNotFoundInTreeException();
        }
        // if both children of root are black, set root to red
        if (!Node.isRed(root.left) && !Node.isRed(root.right))
            root.setColor(RED);

        root = delete(root, index);
        if (!isEmpty())
            root.setColor(BLACK);
        // assert check();
    }

    // delete the key-value pair with the given key rooted at h
    private Node delete(Node h, int index) {
        // assert get(h, key) != null;

        if (  index < h.item.index )  {
            if (!Node.isRed(h.left) && !Node.isRed(h.left.left))
                h = moveRedLeft(h);
            h.left = delete(h.left, index);
        }
        else {
            if (Node.isRed(h.left))
                h = rotateRight(h);
            if (index == h.item.index && (h.right == null))
                return null;
            if (!Node.isRed(h.right) && !Node.isRed(h.right.left))
                h = moveRedRight(h);
            if ( index == h.item.index ) {
                Node x = min(h.right);
                h.item.index = x.item.index;
                h.item.value = x.item.value;
                // h.item.value = get(h.right, min(h.right).item.index);
                // h.item.index = min(h.right).item.index;
                h.right = deleteMin(h.right);
            }
            else h.right = delete(h.right, index);
        }
        return balance(h);
    }




    // Assuming that h is red and both h.left and h.left.left
    // are black, make h.left or one of its children red.
    private Node moveRedLeft(Node h) {
        // assert (h != null);
        // assert isRed(h) && !isRed(h.left) && !isRed(h.left.left);

        flipColors(h);
        if (Node.isRed(h.right.left)) {
            h.right = rotateRight(h.right);
            h = rotateLeft(h);
            flipColors(h);
        }
        return h;
    }

    // Assuming that h is red and both h.right and h.right.left
    // are black, make h.right or one of its children red.
    private Node moveRedRight(Node h) {
        // assert (h != null);
        // assert isRed(h) && !isRed(h.right) && !isRed(h.right.left);
        flipColors(h);
        if (Node.isRed(h.left.left)) {
            h = rotateRight(h);
            flipColors(h);
        }
        return h;
    }

    // restore red-black tree invariant
    private Node balance(Node h) {
        // assert (h != null);

        if (Node.isRed(h.right)) {
            h = rotateLeft(h);
        }
        if (Node.isRed(h.left) && Node.isRed(h.left.left)) {
            h = rotateRight(h);
        }
        if (Node.isRed(h.left) && Node.isRed(h.right)) {
            flipColors(h);
        }

        h.setSize(Node.getSize(h.left) + Node.getSize(h.right) + 1);
        return h;
    }

    // the smallest key in subtree rooted at x; null if no such key
    private Node min(Node x) {
        // assert x != null;
        if (x.left == null) {
            return x;
        }
        else {
            return min(x.left);
        }
    }


    public double[] getAll(){
        List<Double> result = new ArrayList<>();
        result = getAll(root);
        return result.stream().mapToDouble(d->(double)d).toArray();
    }
    private List<Double> getAll(Node node){
        if ( node == null ){
            return new ArrayList<Double>();
        }
        List<Double> result = getAll(node.left);
        result.add(node.item.value);
        result.addAll( getAll(node.right));
        return result;
    }

}
