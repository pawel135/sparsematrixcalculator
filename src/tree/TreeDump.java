package tree;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TreeDump {
    static int bottomNodeRepresentationLength = 6;
    final static char elementSeparator = '|';
    final static String paddingStr = " ";

    public static void dump(Tree tree) {
        dump(tree.getRoot(),0);
    }

    private static void dump(Node node, int depth) {
        if (node == null) {
            return;
        }
        depth++;
        dump(node.left,depth);
        System.out.println("index: " + node.item.index + ", value: " +  node.item.value + ", depth: " + depth);
        dump(node.right,depth);
        depth--;
    }


    public static void dumpEachLevel(Tree tree){
        if ( tree.getRoot() == null ){
            return;
        }
        int bottomNodeRepresentationLengthOld;
        do {
            bottomNodeRepresentationLengthOld = bottomNodeRepresentationLength;
            try {
                System.out.println(dumpEachLevel(tree.getRoot(), 0, tree.getDepth()));
            } catch (IllegalArgumentException e) {
                bottomNodeRepresentationLength++;
            }
        } while ( bottomNodeRepresentationLength != bottomNodeRepresentationLengthOld );
    }


    private static String dumpEachLevel(Node node, int currentDepth, int treeDepth) throws IllegalArgumentException{
        currentDepth++;
        if ( null == node ){
            StringBuilder sb = new StringBuilder();
            for ( int i = 0; i <= treeDepth-currentDepth; i++ ){
                for ( int j = (int)Math.pow(2,i); j < (int)Math.pow(2,i+1); j++){
                    sb.append( getEmptyNodeRepresentation(currentDepth+i-1,treeDepth) );
                }
                sb.append("\n");
            }
            return sb.toString();
        }

        String result;
        String [] levelsLeft;
        String [] levelsRight;

        levelsLeft = dumpEachLevel(node.left,currentDepth,treeDepth).split("\n");
        levelsRight = dumpEachLevel(node.right,currentDepth,treeDepth).split("\n");
        String thisNodeIndex = String.valueOf( node.item.index );

        result = IntStream.range(0,Math.max(levelsLeft.length,levelsRight.length)).
                mapToObj(i->( levelsLeft[i] + levelsRight[i] ))
                .collect(Collectors.joining("\n"));

       try{
            result = formatAlignCenter(currentDepth, treeDepth, thisNodeIndex)+"\n" + result;
        } catch (IllegalArgumentException e) {
            throw e;
        }
        currentDepth--;
        return result;
    }

    private static String getEmptyNodeRepresentation(int currentDepth, int treeDepth) {
        return String.format("%" + (bottomNodeRepresentationLength * (int)Math.pow(2,treeDepth-currentDepth-1) - 1) + "s"+String.valueOf(elementSeparator),"");
    }

    private static String formatAlignCenter(int currentDepth, int treeDepth, String strValue) throws IllegalArgumentException{
        int totalLength = bottomNodeRepresentationLength * (int)Math.pow(2,treeDepth-currentDepth) - 1;
        if (totalLength < strValue.length()){
            throw new IllegalArgumentException();
        }
        int firstPart = (totalLength - strValue.length()) / 2;
        int secondPart = totalLength - firstPart - strValue.length();
        return separator(paddingStr, firstPart) + strValue + separator(paddingStr, secondPart) + String.valueOf(elementSeparator);
    }

    private static String separator(String symbol, int occurrences) {
        StringBuilder sb = new StringBuilder();
        for ( int i = 0; i < occurrences; i++) {
            sb.append(symbol);
        }
        return sb.toString();
    }

}
