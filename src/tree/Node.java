package tree;

class Node {
    Node left;
    Node right;
    Item item;
    private boolean color;
    private int size; // subtree count
    Node(int index, double value, boolean color, int size){
        item = new Item(index,value);
        left = null;
        right = null;
        this.color = color;
        this.size = size;
    }



    public boolean getColor(){
        return color;
    }

    public void setColor(boolean color){
       this.color = color;
    }
    public void revertColor(){
        color = !color;
    }


    public void setSize(int size){
       this.size = size;
    }

    public static boolean isRed(Node node){
        return node != null && node.color == Tree.RED;
    }

    public static int getSize(Node node){
        return node == null ? 0 : node.size;
    }


}
